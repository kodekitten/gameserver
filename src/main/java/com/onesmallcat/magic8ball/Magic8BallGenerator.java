package com.onesmallcat.magic8ball;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;

/**
 * Created by caitlinw on 3/14/16.
 */

public class Magic8BallGenerator {
    private List<String> responses = Lists.newArrayList("Signs point to yes.",
            "Yes.",
            "Reply hazy, try again.",
            "Without a doubt.",
            "My sources say no.",
            "As I see it, yes.",
            "You may rely on it.",
            "Concentrate and ask again.",
            "Outlook not so good.",
            "It is decidedly so.",
            "Better not tell you now.",
            "Very doubtful.",
            "Yes - definitely.",
            "It is certain.",
            "Cannot predict now.",
            "Most likely.",
            "Ask again later.",
            "My reply is no.",
            "Outlook good.",
            "Don't count on it.");


    private Random random = new Random();
    private int min = 0;
    private int max;

    public Magic8BallGenerator() {
        max = responses.size() -1;
    }

    public String generateResponse() {
        return responses.get(random.nextInt((max - min) + 1) + min);

    }

}
