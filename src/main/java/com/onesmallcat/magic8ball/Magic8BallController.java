package com.onesmallcat.magic8ball;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by caitlinw on 3/11/16.
 */

@RestController("/magic8ball")
public class Magic8BallController {
    Magic8BallGenerator generator = new Magic8BallGenerator();


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getMagic8Ball() {
        return ResponseEntity.ok(generator.generateResponse());
    }
}
