package com.onesmallcat.magic8ball;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by caitlinw on 3/14/16.
 */
public class Magic8BallGeneratorTest {
    Magic8BallGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new Magic8BallGenerator();

    }

    @Test
    public void testGenerateResponse() throws Exception {
        assertNotNull("GenerateResponse returns a message", generator.generateResponse());

    }

}